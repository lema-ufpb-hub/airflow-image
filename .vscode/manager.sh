#!/bin/bash
PARAMS=""
# handle params
while (( "$#" )); do
  case "$1" in
    -a|--action)
      shift
      ;;
    *)
      PARAMS="$1"
      shift
      ;;
  esac

done
# set positional arguments in their proper place
eval set -- "$PARAMS"

# Conditionals
if [ "$PARAMS" == "Build Docker Image" ]; then
  echo "Build a new image"
  docker build --no-cache . -t airflow:test
  echo "New image airflow:test"
  docker images
fi

if [ "$PARAMS" == "Check for vunerabilities" ]; then
  echo "Clear trivy cache"
  time trivy image --clear-cache
  echo "Update database"
  time trivy image --download-db-only
  echo "Run image security scan"
  time trivy image airflow:test 
  time trivy image airflow:test --severity CRITICAL 
fi




