# Apache Airflow

Esse projeto faz build de imagem docker para Apache Airflow.
A imagem é customizada com várias bibliotecas python para aplicação em projetos tutelados pelo LEMA-UFPB.

```bash
docker run -it airflow:test bash -c 'pip freeze'
docker run -it airflow:test bash -c 'java --version'
```
